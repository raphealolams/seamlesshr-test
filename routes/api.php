<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/register', 'Api\RegisterController@register');
Route::post('/login', 'Api\RegisterController@login');
Route::post('/createCourseFactory', 'Api\CourseController@createCourseFactory');
Route::get('/exportCoursesAsFile', 'Api\CourseController@exportCoursesAsFile');

Route::group(['middleware' => ['jwt.verify']], function() {
    Route::post('/courseRegistration', 'Api\CourseController@registerCourse');
    Route::get('/me', 'Api\RegisterController@getAuthUser');
    Route::get('/getCourses', 'Api\CourseController@getCourses');
    // Route::get('/exportCoursesAsFile', 'Api\CourseController@exportCoursesAsFile');
});
