<?php


namespace App\Http\Controllers\API;


use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\User;
use Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;


class RegisterController extends BaseController
{

    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $body = $request->json()->all();

        $validator = Validator::make($body, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required',
            'confirmPassword' => 'required|same:password',
        ]);


        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors(), 400);       
        }

        $body['password'] = bcrypt($body['password']);
        $user = User::create($body);

        $success['accessToken'] = JWTAuth::fromUser($user);
        $success['user'] = $user;
        return $this->sendResponse($success, 'User register successfully.', 201);
    }

    public function login(Request $request) 
    {
        $body = $request->json()->all();

        $validator = Validator::make($body, [
            'email' => 'required|email',
            'password' => 'required'
        ]);


        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors(), 400);       
        }

        try {
            if (! $token = JWTAuth::attempt($body)) {
                return $this->sendError('Validation Error.', ['error' => 'Unauthorized'], 401);
            }
        } catch (JWTException $e) {
            return $this->sendError('Validation Error.', ['error' => 'could_not_create_token'], 500);
        }

        $success['accessToken'] = $token;

        return $this->sendResponse($success, 'User login successfully.', 200);
    }

    public function getAuthUser(Request $request)
    {

        try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return $this->sendError('Validation Error.', ['error' => 'user not found'], 404);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return $this->sendError('Validation Error.', ['error' => 'token expired'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return $this->sendError('Validation Error.', ['error' => 'Invalid Token'], $e->getStatusCode());
        
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return $this->sendError('Validation Error.', ['error' => 'Invalid Token'], $e->getStatusCode());
        }

        return $this->sendResponse($user, 'User fetched successfully.', 200);
    }

    public function logout()
    {
        JWTAuth::logout();
        return $this->sendResponse([], 'Successfully logged out', 200);
    }
}