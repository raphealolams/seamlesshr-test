<?php


namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use App\Exports\ExcelExport;
use App\Http\Controllers\API\BaseController as BaseController;
use App\CourseRegistration;
use App\Course;
use Validator;
use App\Jobs\CreateCourse;

class CourseController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCourses(Request $request)
    {
        $courses = Course::all();

        return $this->sendResponse($courses->toArray(), 'Courses retrieved successfully.');
    }

    public function createCourseFactory(Request $request) 
    {
        $this->dispatch((new CreateCourse())->delay(60 * 5));
        return $this->sendResponse([], 'Course Creation Successful.', 201);
    }

    public function registerCourse(Request $request)
    {
        $body = $request->json()->all();
        $userId = $request->user()->id;


        $validator = Validator::make($body, [
            'course_id' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors(), 400);       
        }


        $body['user_id'] = $userId;

        $course = CourseRegistration::create($body);

        return $this->sendResponse($course, 'Course Registration Successful.', 201);

    }

    public function exportCoursesAsFile(Request $request)
    {
        return (new ExcelExport)->download('smealesshr.xlsx');
    } 
}