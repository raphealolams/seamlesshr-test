<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Course;
use Faker\Factory as Faker;


class CreateCourse implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $faker = Faker::create();

        foreach (range(1,50) as $index) {
	        Course::create([
	            'text' => $faker->text,
	            'course_title' => 'STA'.$faker->randomDigit,
                'course_unit' => $faker->randomDigit,
                'course_tutor' => $faker->name,
	        ]);
	    }
    }
}
