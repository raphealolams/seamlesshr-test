<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable = [
        'text', 'course_title', 'course_tutor', 'course_unit'
    ];
}
